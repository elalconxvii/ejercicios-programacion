#include <stdio.h>

//Ejercicio 07: Calculo del Perimetro de Diferentes Triangulos 
//Codigo Fuente en C

float equilatero(float lado, float perimetro){
	perimetro=lado*3;
	return perimetro;
	}
float isosceles(float lado, float base, float perimetro){
	perimetro=(lado*2)+base;
	return perimetro;
	}
float escaleno(float lado_chiquito, float lado_grande, float base, float perimetro){
	perimetro=lado_chiquito+lado_grande+base;
	return perimetro;
	}

int main (){
	int opcion;
	float base, lado, lado_chiquito, lado_grande, perimetro;
	char calcular ='c';

	while(calcular!='e'){
		printf("Presiona 1 para calcular el perimetro de un triangulo equilatero. \nPresiona 2 para calcular el perimetro de un triangulo isosceles. \nPresiona 3 para calcular el perimetro de un triangulo escaleno");
		scanf("%d", &opcion);

			if (opcion==1){
				printf("\nEl triangulo equilatero tiene todos los lados iguales. \nPor favor, ingresa la medida de cualquier lado para calcular el perimetro");
				scanf("%f", &lado);
				perimetro=equilatero(lado, perimetro);
				printf("El perimetro del triangulo equilatero con la medida ingresada por lado es de: %.2f", perimetro);
			}
			
			if (opcion==2){
				printf("\nEl triangulo isosceles tiene dos de sus lados iguales. \nPor favor, ingresa la medida de la base");
				scanf("%f", &base);
				printf("\nPor favor, ingresa la medida de cualquiera de los dos lados restantes");
				scanf("%f", &lado);
				perimetro=isosceles(lado, base, perimetro);
				printf("\nEl perimetro del triangulo isosceles con las medidas ingresadas es de: %.2f", perimetro);
			}
			
			if (opcion==3){
				printf("El triangulo escaleno tiene todos sus lados diferentes. \n Por favor, ingresa la medida del cateto adyacente a la hipotenusa (la base)");
				scanf("%f", &base);
				printf("\nPor favor, ingresa la medida del cateto opuesto a la hipotenusa (el lado mas chiquito)");
				scanf("%f", &lado_chiquito);
				printf("\nPor favor, ingresa la medida de la hipotenusa (el lado mas grande)");
				scanf("%f", &lado_grande);
				perimetro=escaleno(lado_chiquito, lado_grande, base, perimetro);
				printf("\nEl perimetro del triangulo escaleno con las medidas ingresadas es de: %.2f", perimetro);		
			}
		
	printf("\n\n Presiona c para calcular el perimetro de otro triangulo. Presiona e para salir");
    	scanf("%s", &calcular);
	printf("\n\n");		
	}
	
	return 0;	
}
