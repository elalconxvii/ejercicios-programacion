# Ejercicios del curso "Introducción a la programación"

1. Las aplicaciones ejecutables de cada ejercicio fueron especificadas para ignorarse en el archivo .gitignore.
2. En cada carpeta se encuentra un archivo *.txt* con el código fuente desarrollado en C y un archivo tipo *C++ Source File*.

## Gabriela Castro Benítez
Agosto, 2020