#include <stdio.h>

//Ejercicio 12: Fechas Validas en el Calendario
//Codigo Fuente en C

int main(){
	int dia, mes, year;
	char comprobar='c';

	while (comprobar!='e') {
		printf("Para comprobar si una fecha es real (dia/mes/anio), ingresa los siguientes datos");
		
		printf("\nPor favor ingresa el dia en formato numerico (ejemplo: 12, 1, 31)\n");
		scanf("%d", &dia);
		printf("\nPor favor ingresa el mes en formato numerico (ejemplo: 9, 11, 1)\n");
		scanf("%d", &mes);
		printf("\nPor favor ingresa el anio en formato numerico (ejemplo: 1997, 2012, 2020)\n");
		scanf("%d", &year);

		if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12){
			if (dia>=1 && dia<=31){
				printf("VERDADERO");
			}
			else{
				printf("FALSO" );
			}
		}
		
		else if (mes==4 || mes==6 | mes==9 || mes==11){
			if (dia>=1 && dia<=30){
				printf("VERDADERO");
			}
			else{
				printf("FALSO");
			}
		}
		
		else if (mes==2){
			if (year%4==0 && year%100!=0){
				if (dia>=1 && dia<=29){
					printf("VERDADERO");
				}
				else{ 
					printf("FALSO");
				}
			}
			else{
				if (dia>=1 && dia<=28){
					printf("VERDADERO");
				}
				else{
					printf("FALSO");
				}
			}
		}
		
		else if (mes>12){
			printf("FALSO");
		}
		
		printf("\nPresiona c para comprobar si otra fecha es real. Presiona e para salir");
		scanf("%s", &comprobar);
		printf("\n\n");
	}	
	return 0;
}
