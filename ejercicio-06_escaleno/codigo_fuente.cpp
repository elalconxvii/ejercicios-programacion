#include <stdio.h>

//Ejercicio 06: Calculo del Perimetro de un Triangulo Escaleno
//Codigo Fuente en C

int main (){
	float base, lado_chiquito, lado_grande, perimetro;
	char calcular='c';

	while(calcular!='e'){
		printf("Para calcular el perimetro de un triangulo escaleno necesitas la medida de su base y de sus dos lados. Ingresa los datos requeridos para calcularlo.\n\n Por favor, ingresa la medida del cateto adyacente a la hipotenusa (la base)");
		scanf("%f", &base);
		printf("Por favor, ingresa la medida del cateto opuesto a la hipotenusa (el lado m�s chiquito)");
		scanf("%f", &lado_chiquito);
		printf("Por favor, ingresa la medida de la hipotenusa (el lado m�s grande)");
		scanf("%f", &lado_grande);
		perimetro=lado_grande+lado_chiquito+base;
		printf("\nEl perimetro del triangulo escaleno con las medidas ingresadas es de: %.2f", perimetro);
		
		printf("\n\n Presiona c para calcular el perimetro de otro triangulo escaleno. Presiona e para salir");
	    	scanf("%s", &calcular);
		printf("\n\n");		
	}
	
	return 0;
}
