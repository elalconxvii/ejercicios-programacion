#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Ejercicio 15: Juego de Volados
//Codigo Fuente en C

int main (){
	int user_bet, cpu_bet, volado, guess, user_final, cpu_final;
	int user_money=0;
	int cpu_money=0;
	int user_score=0;
	int cpu_score=0;
	int i=1;
	char jugar='j';
	
	srand(time(NULL));
	
	while (jugar!='e') {
		printf("Bill: Hola, amigo.\Te gustaria jugar conmigo?\n Jugaremos volados.\n\nREGLAS:\nIniciamos con $500 de bolsa.\nCada ronda debes hacer una apuesta entre $20 y $100, solo cantidades enteras.\nHay tres rondas, el que gane mas se llevara su dinero y las apuestas del otro.\n\nEstas listo? Empecemos!");
		
		for (i==1; i<=3; i++){
			printf("\n\nRonda %d", i);
			
			cpu_bet=20+rand()%(101-20);
			printf("\nBill: Yo ya hice mi apuesta, amigo. Ingresa la tuya:");
			scanf ("%d",&user_bet);
			
			volado=rand()%1;
			
			printf("\nBill: Si crees que salio aguila escribe 0, si crees que salio sol escribe 1");
			scanf("%d", &guess);
			
				if(volado==guess){
					printf("Bill: Maravilloso, has ganado esta ronda");
					user_score=user_score+1;
					user_money=user_money+cpu_bet;
					cpu_money=cpu_money-cpu_bet;
				}
				else if (volado!=guess){
					printf("Bill: Lo siento, amigo, te has equivocado. Yo gano esta ronda");
					cpu_score=cpu_score+1;
					cpu_money=cpu_money+user_bet;
					user_money=user_money-user_bet;
				}
		}
		
		if(user_score>cpu_score){
			user_final=500+user_money;
			cpu_final=500+cpu_money;
			i=1;
			printf("\n\n\t Marcador final del usuario: %d %s %d", user_score, "\n\tMarcador final de la computadora: ", cpu_score);
			printf("\n\t Bolsa acomulada del usuario: %d %s %d", user_final, "\n\tBolsa acomulada de la computadora: ", cpu_final);
			printf("\nBill: Hemos terminado el juego.\nTu has ganado, amigo, felicidades. Ahora puedes pedir un deseo");
		}			
		
		else if (user_score<cpu_score){
			user_final=500+user_money;
			cpu_final=500+cpu_money;
			i=1;
			printf("\n\n\t Marcador final del usuario: %d %s %d", user_score, "\n\tMarcador final de la computadora: ", cpu_score);
			printf("\n\t Bolsa acomulada del usuario: %d %s %d", user_final, "\n\tBolsa acomulada de la computadora: ", cpu_final);
			printf("\nBill: Hemos terminado el juego.\nLo siento, amigo, yo he ganado. Tu deseo me pertenece, asi como tu dinero");
		}
		
		printf("\n\nPresiona j para jugar conmigo de nuevo. Presiona e para salir");
		scanf("%s", &jugar);
		printf("\n\n");
	}
	return 0;	
}
