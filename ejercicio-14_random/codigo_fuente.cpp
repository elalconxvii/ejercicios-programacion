#include <stdio.h>
#include <stdlib.h>
#include <time.h>>

//Ejercicio 14: Juego para Adivinar el Numero
//Codigo Fuente en C

int main(){
	int num_random, num_ingresado;
	int intento=1;
	int t=time(NULL);
	char jugar='j';
	
	srand(t);
	
	while (jugar!='e'){
		printf("Hola, amigo.\nTendras 5 intentos para adivinar el numero, del 1 al 100, en el que estoy pensando y ganar.\n");
		
		do {
			num_random=rand()%100;
			
			printf("Piensa bien: �en que numero estoy pensando?\n");
			scanf("%d", &num_ingresado);
			
			if(num_ingresado!=num_random && intento!=5){
				printf("Has perdido una de tus oportunidades. Intenta de nuevo\n");
				intento=intento+1;
			}
			
			else if (num_ingresado==num_random){
				intento=0;
			}	
		} while (intento>=1 && intento<=5);
		
		if(intento==0){
			printf("\nQue ser tan inteligente eres. Haz ganado, ahora puedes pedir un deseo.\nPresiona j para jugar conmigo de nuevo. Presiona e para salir");
				scanf("%s", &jugar);		
		}
				
		if (intento==5){
			printf("Tus oportunidades se agotaron, amigo. Hoy no tendras tu deseo.\nPresiona j para jugar conmigo de nuevo. Presiona e para salir");
			scanf("%s", &jugar);
		}
	}
	return 0;
}
