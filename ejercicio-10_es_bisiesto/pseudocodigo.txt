Pseudocodigo: Ejercicio 10

INICIO
	Numerico Entero year
	Caracter comprobar='c'

	While (comprobar!='e') hacer
		Escribir "Se puede saber si un año sera bisiesto si la fecha es divisible entre 4 y 400, resultando en un residuo igual a 0, pero no lo es entre 100."
		Leer "Ingresa el año que desees verifircar", year
		
			Si (year/4==0 AND year/100!=0 OR year/400==0) hacer
				Escribe "El año ingresado: ", year, "SI es bisisesto."
			Si No 
				Escribe "El año ingresado: ", year, "NO es bisiesto."
			FIN Si-No

		Leer "Presiona c para comprobar, de nuevo, si otro año sera bisiesto. Presiona e para salir", comprobar 
	FIN While
FIN