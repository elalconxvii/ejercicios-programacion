#include <stdio.h>

//Ejercicio 04: Calculo del Perimetro de un Triangulo Equilatero
//Codigo Fuente en C

int main (){
	float lado, perimetro;
	char calcular='c';

	while(calcular!='e'){
		printf("Para calcular el perimetro de un triangulo necesitas el valor de uno de sus lados. \nPor favor, ingresa la medida de cualquier lado para calcularlo");
		scanf("%f", &lado);
		perimetro=lado*3;
		printf("\nEl perimetro del triangulo equilatero con la medida ingresada por lado es de: %.2f", perimetro);
				
		printf("\n\n Presiona c para calcular el perimetro de otro triangulo equilatero. Presiona e para salir");
	    scanf("%s", &calcular);
	    printf("\n\n");
	}
	
	return 0;
}
